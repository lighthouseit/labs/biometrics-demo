/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useRef } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  Button,
  Platform,
  Modal,
} from 'react-native';
import { Header } from 'react-native/Libraries/NewAppScreen';
import * as LocalAuthentication from 'expo-local-authentication';
import LottieView from 'lottie-react-native';

import FingerprintSuccess from './assets/fingerprint_success.json';
import FingerprintError from './assets/fingerprint_error.json';

const App: () => React$Node = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [failedCount, setFailedCount] = useState(0);
  const fingerprintAnimation = useRef();

  const scanFingerPrint = async () => {
    setAuthenticated(false);

    if (Platform.OS === 'android') {
      fingerprintAnimation.current.play(0, 115);
    }

    try {
      let results = await LocalAuthentication.authenticateAsync();
      if (results.success) {
        setFailedCount(0);
        setAuthenticated(true);
        fingerprintAnimation.current.play(115, 238);
      }
      if (results.error && results.error !== 'user_cancel') {
        setFailedCount(failedCount + 1);
        setAuthenticated(false);
        fingerprintAnimation.current.play(115, 238);
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Header />
        <Button
          title="Entrar"
          onPress={() => {
            if (Platform.OS === 'android') {
              LocalAuthentication.cancelAuthenticate();
              setModalVisible(!modalVisible);
            } else {
              scanFingerPrint();
            }
          }}
        />

        {authenticated && (
          <Text style={styles.text}>Authentication Successful! 🎉</Text>
        )}

        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onShow={scanFingerPrint}>
          <View style={styles.modal}>
            <View style={styles.innerContainer}>
              <Text style={styles.title}>Autenticar na Panvel</Text>

              <LottieView
                ref={fingerprintAnimation}
                source={failedCount > 0 ? FingerprintError : FingerprintSuccess}
                style={styles.fingerprint}
                autoPlay={false}
                loop={false}
              />

              {failedCount > 0 && failedCount < 5 && (
                <Text style={styles.failed}>
                  Falha na autenticação, tente novamente.
                </Text>
              )}

              {failedCount >= 5 && (
                <Text style={styles.failed}>
                  Você falhou 5 vezes, desbloqueie seu celular e tente
                  novamente.
                </Text>
              )}
              {authenticated && (
                <Button
                  title="Continuar"
                  color="#183d8e"
                  onPress={async () => {
                    LocalAuthentication.cancelAuthenticate();
                    setModalVisible(!modalVisible);
                  }}
                />
              )}

              {!authenticated && (
                <Button
                  title={failedCount > 0 ? 'Tentar novamente' : 'Cancelar'}
                  color="#da1d52"
                  onPress={async () => {
                    LocalAuthentication.cancelAuthenticate();

                    if (failedCount > 0) {
                      scanFingerPrint();
                    } else {
                      setModalVisible(!modalVisible);
                    }
                  }}
                />
              )}
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderRadius: 12,
    backgroundColor: '#FFF',
  },
  footer: {
    alignSelf: 'stretch',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 16,
  },
  fingerprint: {
    width: 128,
    height: 128,
  },
  failed: {
    color: '#da1d52',
    fontSize: 14,
    paddingBottom: 16,
    textAlign: 'center',
  },
  text: {
    alignSelf: 'center',
    fontSize: 22,
    paddingTop: 20,
  },
});

export default App;
